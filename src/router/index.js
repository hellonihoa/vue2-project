import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/layout/index.vue'
import Login from '@/views/login/index.vue'
import Home from '@/views/home/index.vue'

// 产品
const Product = ()=>import('@/views/product/index.vue')
const List = ()=>import('@/views/product/list/index.vue')
const Category = ()=>import('@/views/product/category/index.vue')

//订单
const Order =()=>import('@/views/order/index.vue')
const Collect =()=>import('@/views/order/collect/index.vue')
const OrderList =()=>import('@/views/order/orderList/index.vue')
const Review =()=>import('@/views/order/review/index.vue')


//广告
const Advert =()=>import('@/views/advert/index.vue')
const JmList =()=>import('@/views/advert/jmList/index.vue')

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    component:Layout,
    children:
    [
      {
        path:'/',
        naem:'name',
        component:Home,
      },
      {
        path:'/product', //产品
        name:'product',
        component:Product,
        children:[
          {
            path:'list',
            name:'list',
            component:List,
          },
          {
            path:'/category',
            name:'category',
            component:Category,
          }
        ]
      },
      {
        path:'/order', //订单
        name:'order',
        component:Order,
        children:[
          {
            path:'/collect',
            name:'collect',
            component:Collect,
          },
          {
            path:'/orderList',
            name:'orderList',
            component:OrderList,
          },
          {
            path:'/review',
            name:'review',
            component:Review,
          }
        ]
      },
      {
        path:'/advert', //广告
        name:'advert',
        component:Advert,
        children:[
          {
            path:'/jmList',
            name:'jmList',
            component:JmList,
          },
        ]
      }
    ]
  },
  // 登录
  {
    path:'/login',
    name:'login',
    component:Login,
  },
]

const router = new VueRouter({
  routes
})

export default router
